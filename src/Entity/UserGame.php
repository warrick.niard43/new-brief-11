<?php

namespace App\Entity;

use App\Repository\UserGameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserGameRepository::class)
 */
class UserGame
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $bought_at;

    /**
     * @ORM\ManyToOne(targetEntity=game::class, inversedBy="userGames")
     */
    private $game;

    /**
     * @ORM\ManyToMany(targetEntity=platform::class, inversedBy="userGames")
     */
    private $platform;

    public function __construct()
    {
        $this->platform = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBoughtAt(): ?\DateTimeInterface
    {
        return $this->bought_at;
    }

    public function setBoughtAt(\DateTimeInterface $bought_at): self
    {
        $this->bought_at = $bought_at;

        return $this;
    }

    public function getGame(): ?game
    {
        return $this->game;
    }

    public function setGame(?game $game): self
    {
        $this->game = $game;

        return $this;
    }

    /**
     * @return Collection|platform[]
     */
    public function getPlatform(): Collection
    {
        return $this->platform;
    }

    public function addPlatform(platform $platform): self
    {
        if (!$this->platform->contains($platform)) {
            $this->platform[] = $platform;
        }

        return $this;
    }

    public function removePlatform(platform $platform): self
    {
        $this->platform->removeElement($platform);

        return $this;
    }
}
